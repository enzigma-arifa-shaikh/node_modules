export class ArrayClass
{
     trainees:string[]; 
     
    constructor()
    {
        this.trainees = ['sam', 'ram', 'banana'] ;
        
    }
    
    public newTrainee(newJoinser : string):string[]
    {
        var alphas:string[]= this.trainees; 
        alphas.push(newJoinser);
        return alphas;
    }
    public  display(): void
    {
        console.log(this.trainees);

    }
    public  noOfTrainees(): number{
       let a = this.trainees.length;
        return a;
    }
    public addAtTop(newJoinser : string):string[]
    {
        var alphas:string[]= this.trainees; 
        alphas.unshift(newJoinser);
        return alphas;
    }
    public removeTrainee(): String[]
    {
        var alphas:string[]= this.trainees; 
        alphas.shift(); 
        return alphas;
    }
    public  sortTrainee() : String[]
    {
        var alphas:string[]= this.trainees;
        var sorted = alphas.sort();
        return  sorted; 
    }

}
/*var newtrainee:string[];
let numberOfTrainee;
let obj = new ArrayClass();
obj.display();
newtrainee = obj.newTrainee('aru');
console.log(newtrainee);
numberOfTrainee = obj.noOfTrainees();
console.log(numberOfTrainee);
newtrainee = obj.addAtTop('aru');
console.log(newtrainee);
 obj.removeTrainee();
console.log(newtrainee);
obj.sortTrainee();
console.log(newtrainee);*/
//console.log(obj.newTrainee('aru'));
