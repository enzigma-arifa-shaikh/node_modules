"use strict";
exports.__esModule = true;
var training_1 = require("../object/training");
function startTraining(task) {
    training_1.training.traineeName = task;
    training_1.training.trainingstatus = function () {
        return 'Training Done Successfully with Modules' + training_1.training.moduleList;
    };
}
exports.startTraining = startTraining;
function statusTraining() {
    console.log(training_1.training.trainingstatus());
}
exports.statusTraining = statusTraining;
function newModule(module) {
    if (training_1.training.noOfModules == training_1.training.moduleList.length) {
        console.log(training_1.training.trainingstatus());
    }
    else {
        training_1.training.moduleList.push(module);
    }
    console.log(training_1.training.moduleList);
}
exports.newModule = newModule;
/* startTraining('abc');
 newModule('arub');
 newModule('arubv');
 newModule('arubvh');*/ 
