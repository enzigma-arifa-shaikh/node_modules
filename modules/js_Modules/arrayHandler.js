"use strict";
exports.__esModule = true;
var ArrayClass = /** @class */ (function () {
    function ArrayClass() {
        this.trainees = ['sam', 'ram', 'banana'];
    }
    ArrayClass.prototype.newTrainee = function (newJoinser) {
        var alphas = this.trainees;
        alphas.push(newJoinser);
        return alphas;
    };
    ArrayClass.prototype.display = function () {
        console.log(this.trainees);
    };
    ArrayClass.prototype.noOfTrainees = function () {
        var a = this.trainees.length;
        return a;
    };
    ArrayClass.prototype.addAtTop = function (newJoinser) {
        var alphas = this.trainees;
        alphas.unshift(newJoinser);
        return alphas;
    };
    ArrayClass.prototype.removeTrainee = function () {
        var alphas = this.trainees;
        alphas.shift();
        return alphas;
    };
    ArrayClass.prototype.sortTrainee = function () {
        var alphas = this.trainees;
        var sorted = alphas.sort();
        return sorted;
    };
    return ArrayClass;
}());
exports.ArrayClass = ArrayClass;
/*var newtrainee:string[];
let numberOfTrainee;
let obj = new ArrayClass();
obj.display();
newtrainee = obj.newTrainee('aru');
console.log(newtrainee);
numberOfTrainee = obj.noOfTrainees();
console.log(numberOfTrainee);
newtrainee = obj.addAtTop('aru');
console.log(newtrainee);
 obj.removeTrainee();
console.log(newtrainee);
obj.sortTrainee();
console.log(newtrainee);*/
//console.log(obj.newTrainee('aru'));
